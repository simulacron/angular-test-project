import { Component, OnInit } from '@angular/core';
import {ModelService} from "../model.service";
import {SubmodelService} from "../submodel.service";

@Component({
  selector: 'app-debacode',
  templateUrl: './debacode.component.html',
  styleUrls: ['./debacode.component.css']
})
export class DebacodeComponent implements OnInit {

  constructor(private modelService: ModelService, private submodelService: SubmodelService) { }

  ngOnInit(): void {
  }

  button1Click(): void {
    console.log(this.modelService.very_complicated_function())
  }

  button2Click(): void {
     console.log(this.submodelService.get_data())
  }

}
