import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DebacodeComponent } from './debacode.component';

describe('DebacodeComponent', () => {
  let component: DebacodeComponent;
  let fixture: ComponentFixture<DebacodeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DebacodeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DebacodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
