import { TestBed } from '@angular/core/testing';

import { SubmodelService } from './submodel.service';

describe('SubmodelService', () => {
  let service: SubmodelService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SubmodelService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
