
import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit{
  constructor() { }

  // x and y offset of whole system
  offset_x = 10;
  offset_y = 10;
  // size of top circle
  circle_size_start = 7;
  // size of start circles of subtask
  circle_size_big = 4;
  // size of circle closing a subtask
  circle_size_small = 3;
  // width of the lines connecting the circles
  line_width = 2;
  // list of colors used for subtasks, each entry represents one level, wraps around when not enough colors are in the list
  colors = ['blue', 'green', 'orange']
  // color for subtask circle color fill
  color_white = 'white';
  // stroke width of subtask start circle (looks best when it is equal to line_width)
  circle_stroke_width = 2;
  // distance a subtask is moved inwards from last subtask
  depth_step = 15;
  // distance of how far the connection lines between subtasks reach in y-direction
  connection_offset = 10;

  // placeholder for datafileds
  datafield_ids = [0,1,2,3];

  //fields for storing heights/positions  of tasks
  datafield_heights: number[] = [];
  datafield_pos: number[] = [];
  total_height: number = 0;

  // PLEASE NOTE: For the drawing to appear correctly the subtasks need to be ordered from deepest to lowest level
  // This means the lowest subtask comes first in the list and the main parent task is the last in the list
  subtask_list: any = []

  ngAfterViewInit() {
    this.calculateTaskHeights();

    this.addSubtask(2, 0, 3, true, true);
    this.addSubtask(1, 1, 2, false, true);
    this.addSubtask(1, 1, 1, true, false);
  }

  // calculates all heights and positions of tasks
  calculateTaskHeights() {
    let last_height = 0;
    for (let elem in this.datafield_ids) {
      // @ts-ignore
      let height = document.getElementById(elem).offsetHeight;
      this.datafield_heights.push(height);
      this.datafield_pos.push(last_height);
      last_height = last_height + height;
    }
    this.total_height = last_height;
  }

  /*
  * startIndex: the index of the subtask to draw in the ordered tasklist
  * numberOfDataFields: How many datafields of the tasklist should be connected by this subtask line (0 only draws the first datafield)
  * level: Level of the subtask
  * previousTaskHasOtherStepsBefore: If the previous task has done anything beforehand (like filling other datafields or subtasks)
  * this is used to determine wether the connection line can be connected directly or must be moved downwards to account
  * for the offset of the connection line of the parent task
  * isLastSubtask: Same game as for previousTaskHasOtherStepsBefore, but for the end. If the Subtask is the last thing done in the parent
  * task the connection line needs to be moved up accordingly
  *  */
  addSubtask(startIndex: number, numberOfDataFields: number, level: number, previousTaskHasOtherStepsBefore: boolean = false, isLastSubtask: boolean = true){

    let posY1 = this.datafield_pos[startIndex] + this.connection_offset;
    if (!previousTaskHasOtherStepsBefore) {
      posY1 += (level - 1) * this.connection_offset;
    }
    let posX1 = level * this.depth_step + this.offset_x;
    let posY2 = this.datafield_pos[startIndex + numberOfDataFields] + this.datafield_heights[startIndex + numberOfDataFields] - this.connection_offset
    if (isLastSubtask) {
      posY2 -= (level - 1) * this.connection_offset;
    }
    let posX2 = level * this.depth_step + this.offset_x;

    let posXCon1 = posX1 - this.depth_step;
    let posYCon1 = posY1 - this.connection_offset;
    let posXCon2 = posX2 - this.depth_step;
    let posYCon2 = posY2 + this.connection_offset;


    let subtask_object  = {

      // pos of top circle of subtask
      posY1,
      posX1,
      // pos of bottom circle of subtask
      posY2,
      posX2,

      // pos of top connection between subtask and parent task
      posXCon1,
      posYCon1,
      // pos of bottom connection between subtask and parent task
      posXCon2,
      posYCon2,

      // level of the current subtask
      level,
    }

    this.subtask_list.push(subtask_object);
  }

  @ViewChild('myIdentifier')
  myIdentifier: ElementRef | undefined;
}
