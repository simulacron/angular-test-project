import { Injectable } from '@angular/core';
import {SubmodelService} from "./submodel.service";

@Injectable({
  providedIn: 'root'
})
export class ModelService {

  constructor(private submodelService: SubmodelService) { }

  very_complicated_function(): string{

    return this.submodelService.get_data() + ' extra stuff'

  }
}
